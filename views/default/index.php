<?php

    // Без обращения к index - просмотр запрещен
    if (!defined('SYSTEM'))
    {
        die('Не разрешен просмотр');
    }

    $h1 = 'Дневники';

    \xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
        [
            'name' => 'Дневники',
        ],
    ]);

    // Устанавливаем Title страницы
    \xtetis\xengine\helpers\SeoHelper::setTitle(
        'Дневники - ' . 
        APP_NAME
    );


    // Устанавливаем Description страницы
    \xtetis\xengine\helpers\SeoHelper::setDescription(
        'Дневники'
    );

    // Устанавливаем H1 страницы
    \xtetis\xengine\helpers\SeoHelper::setPageName(
        $h1
    );

    
?>

<style>
.card-xform {}

.card-xform a.nav-link.active {
    color: #000 !important;
}

.card-xform a.nav-link {
    color: #fff;
}

</style>


<h3>
    <?=$h1?>
</h3>

