<?php

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}



\xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
    [
        'url'  => $urls['url_cms_main'],
        'name' => 'Админка',
    ],
    [
        'name' => 'Дневники - сообщества',
    ],
]);



?>

<a href="<?=$urls['url_add_community']?>"
class="btn  btn-primary"
style="float: right; margin-bottom: 10px;">
Добавить сообщество
</a>



<div class="table_article_list_container">
    <div class="table-responsive table_article_list_inner_container">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th style="width: 0;">#</th>
                    <th>Название</th>
                    <th>Дата</th>
                    <th>Del</th>
                    <th style="width: 0;"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($model_list as $id => $model): ?>
                <tr <?=($model->deleted) ? ' class="table-danger"' : ''?>>
                    <td><?=$model->id?></td>
                    <td><?=$model->name?></td>
                    <td><?=$model->create_date?></td>
                    <td><?=$model->deleted?></td>
                    <td>
                        <a href="<?=$urls['url_edit_community'][$model->id]?>">
                            <i class="far fa-edit"></i>
                        </a>


                        <?php if ($model->deleted): ?>
                        <a class="cms_article_restore_from_trash"
                           onclick="return confirm('Вернуть из корзины ?');"
                           idx="<?=$model->id?>"
                           href="<?=$urls['url_undelete_community'][$model->id]?>">
                            <i class="fas fa-trash-restore"></i>
                        </a>
                        <?php endif;?>

                        <a class="cms_article_set_deleted"
                           idx="<?=$model->id?>"
                           onclick="return confirm('Удалить ?');"
                           deleted="<?=$model->deleted?>"
                           href="<?=$urls['url_delete_community'][$model->id]?>">
                            <i class="fas fa-trash"></i>
                        </a>

                    </td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>