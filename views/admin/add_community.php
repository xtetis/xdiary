<?php

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}


// Добавляем breadcrumbs
\xtetis\xengine\App::getApp()->setParam('breadcrumbs', [
    [
        'url'  => $urls['url_cms_main'],
        'name' => 'Админка',
    ],
    [
        'url'  => $urls['url_community'],
        'name' => 'Дневники - сообщества',
    ],
    [
        'name' => 'Добавить сообщество',
    ],
]);



?>


<?=\xtetis\xform\Component::renderOnlyFormStart([
    'url_validate' => $urls['url_validate'],
    'form_type'    => 'ajax',
]);?>
<h1 class="mb-3 f-w-400">
    Создать запись
</h1>




<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'Заголовок',
            'name'  => 'name',
        ],
        'value'=>''
    ]
)?>


<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'input_text',
        'attributes' => [
            'label' => 'ЧПУ',
            'name'  => 'sef',
        ],
        'value'=>''
    ]
)?>




<?=\xtetis\xform\Component::renderField(
    [
        'template'   => 'textarea',
        'attributes' => [
            'label' => 'Описание сообщества',
            'name'  => 'description',
            'class' => 'form-control',

        ],
        'value'      => '',
    ]
)?>


<button type="submit"
        class="btn btn-block btn-primary mb-4">Сохранить</button>
<?=\xtetis\xform\Component::renderFormEnd();?>

