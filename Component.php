<?php

namespace xtetis\xdiary;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

class Component extends \xtetis\xengine\models\Component
{

    /**
     * Возвращает массив ссылок для карты сайта
     */
    public static function getSitemapUrlList():array
    {
        /*
        $ret = [
            self::makeUrl(['with_host' => true]),
            self::makeUrl([
                'path'      => [
                    'party',
                ],
                'with_host' => true
            ]),
        ];

        $sql        = 'SELECT `id` FROM `xdate_profile` WHERE COALESCE(`id_profile_type`,0) > 0';
        $model_list = \xtetis\xdate\models\ProfileModel::getModelListBySql($sql);
        foreach ($model_list as $model_date_profile)
        {
            $ret[] = $model_date_profile->getLink(['with_host'=>true]);
        }

        $sql        = 'SELECT id FROM `xgeo_object` WHERE `sef` IS NOT NULL';
        $model_list = \xtetis\xgeo\models\ObjectModel::getModelListBySql($sql);
        foreach ($model_list as $model_geo_object)
        {
            $ret[] = \xtetis\xdate\Component::makeUrl([
                'path'=>[
                    'geo_'.$model_geo_object->sef
                ],
                'with_host'=>true
            ]);

        }

        return $ret;
        */

        return [];
    }

    /**
     * Возвращает список ссылок для менеджера (xcms)
     * так, например эти ссылки будут встроены в админку xcms и будут иметь вид
     * 
     * https://lsfinder.com/manager/component_admin/diary/admin/community
     * и будут требовать в контроллере проверку 
     * 
     * // Без обращения к xcms - работа невозможн
     * if (!defined('ADMIN')
     * {
     *      die('Разрешен просмотр только из XCMS');
     * }
     */
    public static function getManagerActions()
    {
        return [
            [
                'title'  => 'Сообщества',
                'action' => 'admin',
                'query'  => 'community',
            ],
        ];
    }
}
