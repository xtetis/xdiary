<?php

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

// Без обращения к xcms - работа невозможна
if (!defined('ADMIN'))
{
    die('Разрешен просмотр только из XCMS');
}



// Урлы
// ------------------------------------------------
$urls['url_cms_main']  = \xtetis\xcms\Component::makeUrl();
$urls['url_validate']  = \xtetis\xcms\Component::makeUrlAdminComponent(
    \xtetis\xdiary\Component::class,
    'admin',
    'ajax_validate_add_community',
);
$urls['url_community']  = \xtetis\xcms\Component::makeUrlAdminComponent(
    \xtetis\xdiary\Component::class,
    'admin',
    'community',
);
// ------------------------------------------------
// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'                  => $urls,
    ],
);
