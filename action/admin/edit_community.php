<?php

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

// Без обращения к xcms - работа невозможна
if (!defined('ADMIN'))
{
    die('Разрешен просмотр только из XCMS');
}

// Урлы
// ------------------------------------------------
$urls['url_cms_main'] = \xtetis\xcms\Component::makeUrl();
$urls['url_validate'] = \xtetis\xcms\Component::makeUrlAdminComponent(
    \xtetis\xdiary\Component::class,
    'admin',
    'ajax_validate_edit_community',
);
$urls['url_community'] = \xtetis\xcms\Component::makeUrlAdminComponent(
    \xtetis\xdiary\Component::class,
    'admin',
    'community',
);
// ------------------------------------------------

$id = \xtetis\xengine\helpers\RequestHelper::get('id', 'int', 0);

$model = \xtetis\xdiary\models\CommunityModel::generateModelById($id);
if (!$model)
{
    \xtetis\xengine\helpers\LogHelper::customDie('Сообщество не найдено');
}

// ------------------------------------------------
// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'  => $urls,
        'model' => $model,
    ],
);
