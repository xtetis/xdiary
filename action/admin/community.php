<?php

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

// Без обращения к xcms - работа невозможна
if (!defined('ADMIN'))
{
    die('Разрешен просмотр только из XCMS');
}



$model_list = \xtetis\xdiary\models\CommunityModel::getAllModels();

// Урлы
// ------------------------------------------------
$urls['url_cms_main']  = \xtetis\xcms\Component::makeUrl();
$urls['url_add_community'] = \xtetis\xcms\Component::makeUrlAdminComponent(
    \xtetis\xdiary\Component::class,
    'admin',
    'add_community',
);


foreach ($model_list as $id => $model)
{
    $urls['url_edit_community'][$model->id] = \xtetis\xcms\Component::makeUrlAdminComponent(
        \xtetis\xdiary\Component::class,
        'admin',
        'edit_community',
        [
            'id' => $model->id,
        ],
    );
    $urls['url_undelete_community'][$model->id] = \xtetis\xcms\Component::makeUrlAdminComponent(
        \xtetis\xdiary\Component::class,
        'admin',
        'undelete_community',
        [
            'id' => $model->id,
        ],
    );
    $urls['url_delete_community'][$model->id] = \xtetis\xcms\Component::makeUrlAdminComponent(
        \xtetis\xdiary\Component::class,
        'admin',
        'delete_community',
        [
            'id' => $model->id,
        ],
    );
}
// ------------------------------------------------


// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'model_list' => $model_list,
        'urls'                  => $urls,
    ],
);
