<?php

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

// Без обращения к xcms - работа невозможна
if (!defined('ADMIN'))
{
    die('Разрешен просмотр только из XCMS');
}


$response = [
    'result' => false,
];
$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

$model = new \xtetis\xdiary\models\CommunityModel();

// Загружает данные из POST параметров
$model->loadPostData();

if ($model->editCommunity())
{

    $go_to_url = \xtetis\xcms\Component::makeUrlAdminComponent(
        \xtetis\xdiary\Component::class,
        'admin',
        'community',
    );

    $response['result']            = true;
    $response['data']['go_to_url'] = $go_to_url;
}
else
{
    $response['errors'] = $model->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;
