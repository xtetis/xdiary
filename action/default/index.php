<?php

/**
 * Рендер главной страницы компонента
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}
\xtetis\xengine\App::getApp()->setParam('layout', 'list');


// Урлы
// ------------------------------------------------
$urls['url_diary'] = self::makeUrl();

// ------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'             => $urls,
    ],
);
