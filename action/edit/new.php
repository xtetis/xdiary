<?php

/**
 * Рендер страницы добавления поста
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}


// Урлы
// ------------------------------------------------
$urls['url_diary'] = self::makeUrl();

$urls['url_validate'] = self::makeUrl([
    'path'=>[
        'edit',
        'ajax_validate_add_post'
    ]
]);
// ------------------------------------------------

// Рендерим текущую страницу
echo \xtetis\xengine\App::getApp()->renderCurrentPage(
    [
        'urls'             => $urls,
    ],
);
