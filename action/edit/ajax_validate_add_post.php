<?php


/**
 * Валидация при добавлении поста
 */

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

$response = [
    'result' => false,
];
$response['js_success'] = 'xform.goToUrl();';
$response['js_fail']    = '';

$model = new \xtetis\xdiary\models\PostModel();

// Загружает данные из POST параметров
$model->loadPostData();

if ($model->addPost())
{

    $go_to_url = \xtetis\xdiary\Component::makeUrl([
        'path' => [
            'profile',
            $model->id_user
        ],
    ]);

    $response['result']            = true;
    $response['data']['go_to_url'] = $go_to_url;
}
else
{
    $response['errors'] = $model->getErrors();
}

echo \xtetis\xengine\helpers\JsonHelper::arrayToJson($response);
exit;
