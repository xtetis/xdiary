<?php

namespace xtetis\xdiary\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class PostModel extends \xtetis\xengine\models\TableModel
{


    /**
     * @var string
     */
    public $table_name = 'xdiary_post';

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     *
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [
        'id_user' => \xtetis\xuser\models\UserModel::class,
    ];


    /**
     * Название поста	
     */
    public $name = '';

    /**
     * Текст поста	
     */
    public $about = '';

    /**
     * ID пользователя
     */
    public $id_user = 0;

    /**
     * Дата создания записи
     */
    public $create_date = '';


    /**
     * Добавляет пост
     */
    public function addPost()
    {

        if ($this->getErrors())
        {
            return false;
        }

        if (!\xtetis\xuser\Component::isLoggedIn())
        {
            $this->addError('id_user', 'Пользователь не авторизирован');

            return false;
        }

        $this->name      = strval($this->name);
        $this->about     = strval($this->about);
        $this->id_user = \xtetis\xuser\Component::isLoggedIn()->id;

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указано название поста');

            return false;
        }

        if (mb_strlen($this->name) > 150)
        {
            $this->addError('name', 'Название поста не более 150 символов');

            return false;
        }

        if (!strlen($this->about))
        {
            $this->addError('about', 'Не указан текст поста');

            return false;
        }

        if (mb_strlen($this->about) > 10000)
        {
            $this->addError('about', 'Текст поста не может быть больше 10000 символов');

            return false;
        }

        $this->insert_update_field_list = [
            'name',
            'about',
            'id_user',
        ];

        if (!$this->insertTableRecord())
        {
            return false;
        }

        return true;
    }

    /**
     * Редактирует группу
     */
    public function editPost()
    {

        if ($this->getErrors())
        {
            return false;
        }

        /*
        $this->name      = strval($this->name);
        $this->id        = intval($this->id);
        $this->about     = strval($this->about);
        $this->id_parent = intval($this->id_parent);

        $this->about = strip_tags($this->about);

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указан name');

            return false;
        }

        if (mb_strlen($this->name) > 199)
        {
            $this->addError('name', 'Имя группы не может быть больше 199 символов');

            return false;
        }

        if (!$this->id)
        {
            $this->addError('id', 'Не указан ID группы');

            return false;
        }

        $model = self::generateModelById($this->id);
        if (!$model)
        {
            $this->addError('id', 'Группа с id=' . $this->id . ' не существует');

            return false;
        }

        if (!strlen($this->about))
        {
            $this->addError('about', 'Не указан about');

            return false;
        }

        if (mb_strlen($this->about) > 400)
        {
            $this->addError('about', 'Описание группы не может быть больше 400 символов');

            return false;
        }

        if ($this->id_parent)
        {
            $model_parent = self::generateModelById($this->id_parent);
            if (!$model_parent)
            {
                $this->addError('id_parent', 'Родительская группа не существует');

                return false;
            }

            // Выбирать в качестве родительского можно только корень или темы первой вложенности
            if (intval($model_parent->id_parent))
            {
                $this->addError('id_parent', 'Выбирать в качестве родительского можно только корень или темы первой вложенности');

                return false;
            }
        }

        if (!$this->id_parent)
        {
            $this->id_parent = null;
        }

        $this->insert_update_field_list = [
            'name',
            'about',
            'id_parent',
        ];
        if (!$this->updateTableRecordById())
        {
            return false;
        }
        */
        return true;
    }

    /**
     * Сначала помечает пост как удаленный, а потом удаляет пост из БД
     */
    public function deletePost()
    {

        if ($this->getErrors())
        {
            return false;
        }
/*
        $this->deleted = intval($this->deleted);

        if (!$this->deleted)
        {
            $this->deleted                  = 1;
            $this->insert_update_field_list = ['deleted'];
            if (!$this->updateTableRecordById())
            {
                return false;
            }

            return true;
        }
        else
        {
            $this->deleteTableRecordById();

            return true;
        }
        */
    }

    /**
     * Возвращает группу из корзины
     */
    public function undeletePost()
    {

        if ($this->getErrors())
        {
            return false;
        }
        /*
        $this->deleted                  = 0;
        $this->insert_update_field_list = ['deleted'];
        if (!$this->updateTableRecordById())
        {
            return false;
        }
        */
        return true;

    }


    /**
     * Возвращает ссылку на пост
     */
    public function getLink($params = [])
    {
        $with_host = false;
        if (isset($params['with_host']))
        {
            $with_host = $params['with_host'];
        }

        return \xtetis\xdiary\Component::makeUrl([
            'path'  => [
                'post',
                $this->id,
            ],
            'with_host' => $with_host,
        ]);
    }


}
