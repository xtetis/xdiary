<?php

namespace xtetis\xdiary\models;

// Без обращения к index - просмотр запрещен
if (!defined('SYSTEM'))
{
    die('Не разрешен просмотр');
}

/**
 *
 */
class CommunityModel extends \xtetis\xengine\models\TableModel
{

    /**
     * @var string
     */
    public $table_name = 'xdiary_community';

    /**
     * Возвращает связанную модель или false
     * Модели с аттрибутами связываются в массиве related_rule_list, например
     *
     * $this->related_rule_list = [
     *      'attribute_name' => \xtetis\xanyfield\models\ComponentModel::class
     * ];
     *
     * Связанная модель получается методом $this->getModelRelated('attribute_name')
     */
    public $related_rule_list = [
        'id_user' => \xtetis\xuser\models\UserModel::class,
    ];

    /**
     * Название сообщества
     */
    public $name = '';

    /**
     * Урл сообщества
     */
    public $sef = '';

    /**
     * Описание сообщества
     */
    public $description = '';

    /**
     * ID пользователя
     */
    public $id_user = 0;

    /**
     * Дата создания записи
     */
    public $create_date = '';

    /**
     * Опубликовано
     */
    public $published = 0;

    /**
     * Удалено
     */
    public $deleted = 0;

    /**
     * Добавляет сообщество
     */
    public function addCommunity()
    {

        if ($this->getErrors())
        {
            return false;
        }

        if (!\xtetis\xuser\Component::isLoggedIn())
        {
            $this->addError('id_user', 'Пользователь не авторизирован');

            return false;
        }

        $this->name        = strval($this->name);
        $this->sef         = strval($this->sef);
        $this->description = strval($this->description);
        $this->id_user     = \xtetis\xuser\Component::isLoggedIn()->id;

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указано название сообщества');

            return false;
        }

        if (mb_strlen($this->name) > 150)
        {
            $this->addError('name', 'Название сообщества не более 150 символов');

            return false;
        }

        if (!strlen($this->sef))
        {
            $this->addError('sef', 'Не указан урл сообщества');

            return false;
        }

        if (!strlen($this->description))
        {
            $this->addError('description', 'Не указано описание сообщества');

            return false;
        }

        if (mb_strlen($this->description) > 1000)
        {
            $this->addError('about', 'Описание сообщества не может быть больше 1000 символов');

            return false;
        }

        $this->insert_update_field_list = [
            'name',
            'description',
            'sef',
            'id_user',
        ];

        if (!$this->insertTableRecord())
        {
            return false;
        }

        return true;
    }

    /**
     * Редактирует сообщество
     */
    public function editCommunity()
    {

        if ($this->getErrors())
        {
            return false;
        }

        $this->name        = strval($this->name);
        $this->sef         = strval($this->sef);
        $this->description = strval($this->description);
        $this->id        = intval($this->id);

        if (!strlen($this->name))
        {
            $this->addError('name', 'Не указано название сообщества');

            return false;
        }

        if (mb_strlen($this->name) > 150)
        {
            $this->addError('name', 'Название сообщества не более 150 символов');

            return false;
        }

        if (!strlen($this->sef))
        {
            $this->addError('sef', 'Не указан урл сообщества');

            return false;
        }

        if (!strlen($this->description))
        {
            $this->addError('description', 'Не указано описание сообщества');

            return false;
        }

        if (mb_strlen($this->description) > 1000)
        {
            $this->addError('about', 'Описание сообщества не может быть больше 1000 символов');

            return false;
        }

        if (!$this->id)
        {
            $this->addError('id', 'Не указан ID сообщества');

            return false;
        }

        $model = self::generateModelById($this->id);
        if (!$model)
        {
            $this->addError('id', 'Сообщество с id=' . $this->id . ' не существует');

            return false;
        }

        $this->insert_update_field_list = [
            'name',
            'description',
            'sef',
        ];
        if (!$this->updateTableRecordById())
        {
            return false;
        }
        

        return true;
    }

    /**
     * Сначала сообщество пост как удаленное, а потом удаляет сообщество из БД
     */
    public function deleteCommunity()
    {

        if ($this->getErrors())
        {
            return false;
        }
/*
        $this->deleted = intval($this->deleted);

        if (!$this->deleted)
        {
            $this->deleted                  = 1;
            $this->insert_update_field_list = ['deleted'];
            if (!$this->updateTableRecordById())
            {
                return false;
            }

            return true;
        }
        else
        {
            $this->deleteTableRecordById();

            return true;
        }
        */
    }

    /**
     * Возвращает сообщество из корзины
     */
    public function undeleteCommunity()
    {

        if ($this->getErrors())
        {
            return false;
        }
        /*
        $this->deleted                  = 0;
        $this->insert_update_field_list = ['deleted'];
        if (!$this->updateTableRecordById())
        {
            return false;
        }
        */

        return true;

    }

    /**
     * Возвращает ссылку на сообщество
     */
    public function getLink($params = [])
    {
        $with_host = false;
        if (isset($params['with_host']))
        {
            $with_host = $params['with_host'];
        }

        return \xtetis\xdiary\Component::makeUrl([
            'path'      => [
                'community',
                $this->id,
            ],
            'with_host' => $with_host,
        ]);
    }

    /**
     * Возвращает модели всех сообществ
     */
    public static function getAllModels()
    {
        $sql = 'SELECT `id` FROM `xdiary_community` WHERE 1';
        return self::getModelListBySql($sql);
    }

}
